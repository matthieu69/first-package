<?php

namespace Matthieu;

/**
 * HelloWorld class
 *
 */
class HelloWorld
{
    public function __construct()
    {

    }

    public function getMessage()
    {
        return 'Hello World';
    }
}
