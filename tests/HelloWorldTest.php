<?php

namespace Matthieu\Tests;

/**
 * Class HelloWorldTest
 */

class HelloWorldTest extends \PHPUnit_Framework_TestCase
{
    public function testHello()
    {
        $hello = new \Matthieu\HelloWorld();
        $this->assertEquals('Hello World', $hello->getMessage());
    }
}
